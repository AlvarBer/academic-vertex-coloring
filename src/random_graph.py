#!/usr/bin/env python3

from uuid import uuid4
from math import ceil, log
from random import sample
from json import dumps
import re	

def generate_graph(vertices, edges):
	if edges > (vertices * (vertices - 1)) // 2: 
		raise SizeError('Edges shouldn\'t be bigger than n*n+1 / 2')

	graph = {}

	length = ceil(log(vertices, 36)) * 2

	for v in range(vertices):
		name = random_string(length)
		graph[name] = []

	for e in range(edges):
		fst, snd = sample(graph.keys(), 2)
		l = list(graph[fst])
		while snd in l:
			fst, snd = sample(graph.keys(), 2)
			l = list(graph[fst])
		l.append(snd)
		graph[fst] = l
		l = list(graph[snd])
		l.append(fst)
		graph[snd] = l

	return graph

def pretiffy_json(graph):
	json = dumps(graph, sort_keys=True, indent='\t')
	json = re.sub(r'\[\n\t\t','[',json)
	json = re.sub(r'\",\n\t\t','",',json)
	json = re.sub(r'\n\t\]',']',json)
	
	return json

def save_graph(filepath, json):
	file = open(filepath, 'w')

	file.write(json)

	file.close

def random_string(string_length=10):
	"""Returns a random string of length string_length."""
	random = str(uuid4()) # Convert UUID format to a Python string.
	random = random.upper() # Make all characters uppercase.
	random = random.replace("-","") # Remove the UUID '-'.
	return random[0:string_length] # Return the random string.

if __name__ == '__main__':
	graph = generate_graph(10, 10)
	save_graph('../data/small_example.json', pretiffy_json(graph))
	print('Generated small example')

	graph = generate_graph(20, 40)
	save_graph('../data/medium_example.json', pretiffy_json(graph))
	print('Generated medium example')
	
	graph = generate_graph(30, 50)
	save_graph('../data/big_example.json', pretiffy_json(graph))
	print('Generated big example')
