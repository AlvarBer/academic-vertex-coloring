from collections import namedtuple

Node = namedtuple('Node', ['num_colors', 'colored_vertices'])
