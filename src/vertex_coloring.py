from data_structures import Node
from heap import heappush, heappop 
import time

def vertex_coloring(graph, naive=False, metrics=False):
	vertices = list(graph.keys()) # This gives us a list of all vertices
	global total_vertices
	total_vertices = len(vertices) # Num of vertices (global)
	heap = [] # Min Heap we will use to store problem nodes
	nodes_explored = 0 # Number of explored nodes

	root = Node(1, {vertices[0]: 0}) # Root is the first node
	best_cost = pessimistic(root, naive) # On the specification is referred as "y"
	heappush(heap, root) # It could be named y and it wouldn't make a difference
	best_solution = None

	node_time_total = 0

	while len(heap) > 0 and (optimistic(heap[0], naive) < best_cost or
                             best_solution is None and optimistic(heap[0], naive) == best_cost):
		y = heappop(heap)
		node_time = time.perf_counter()
		nodes_explored += 1
		for x in sons(y, vertices):
			if is_solution(x, graph):
				if cost(x) < best_cost or best_solution is None and cost(x) == best_cost:
					best_cost = cost(x)
					best_solution = x
			else:
				if is_completable(x, graph) and ((optimistic(x, naive) < best_cost) or 
				                                 (best_solution is None and optimistic(x, naive) <= best_cost)):
					heappush(heap, x)
					if pessimistic(x, naive) < best_cost:
						best_cost = pessimistic(x, naive)
			node_time_total += node_time

	return best_solution, nodes_explored, (node_time_total/nodes_explored)

def is_solution(node, graph):
	return len(node.colored_vertices) == total_vertices and is_completable(node, graph)

def is_completable(node, graph):
	for vertex in node.colored_vertices:
		adjacents = graph[vertex] # Get adjacent vertices
		adjacent_colors = get_adjacent_colors(node, adjacents)
		if node.colored_vertices[vertex] in adjacent_colors:
			return False

	return True


def cost(node):
	return node.num_colors

def pessimistic(node, naive):
	if naive:
		return total_vertices
	else:
		return node.num_colors + (total_vertices - len(node.colored_vertices))

def optimistic(node, naive):
	if naive:
		return node.num_colors / 2
	else:
		return node.num_colors

# Auxiliary functions

def get_adjacent_colors(node, adjacents):
	adjacent_colors = []
	for adjacent in adjacents: # Get adjacent vertices colors
			try:
				color = node.colored_vertices[adjacent]
				adjacent_colors.append(color)
			except KeyError:
				pass
	return adjacent_colors

def sons(node, vertices):
	if len(node.colored_vertices) == total_vertices:
		return []

	next_vertex = vertices[len(node.colored_vertices)]

	sons_list = []
	for color in range(node.num_colors):
		d = node.colored_vertices.copy()
		d[next_vertex] = color
		sons_list.append(Node(node.num_colors, d))

	d = node.colored_vertices.copy()
	d[next_vertex] = node.num_colors
	sons_list.append(Node(node.num_colors + 1, d))

	return sons_list
