#!/usr/bin/env python3

from vertex_coloring import vertex_coloring
from json import load
from time import time
from datetime import timedelta

# Small Example

file = open('../data/small_example.json')
graph = load(file)

start_time = time()
result = vertex_coloring(graph, naive=True)

print('Time elapsed: {}'.format(timedelta(seconds=(time() - start_time))))
print(result)

# Medium Example

file = open('../data/medium_example.json')
graph = load(file)

start_time = time()
result = vertex_coloring(graph, naive=True)

print('-' * 80)
print('Time elapsed: {}'.format(timedelta(seconds=(time() - start_time))))
print(result)

# Big Example

file = open('../data/big_example.json')
graph = load(file)

start_time = time()
result = vertex_coloring(graph, naive=True)

print('-' * 80)
print('Time elapsed: {}'.format(timedelta(seconds=(time() - start_time))))
print(result)
