#!/usr/bin/env python3

import matplotlib.pyplot as plt

small_x = [10, 10, 10, 10, 10]
medium_x = [20, 20, 20, 20, 20]
big_x = [30, 30, 30, 30, 30]

def plot_nodes(small, medium, big):
	plt.title('Number of explored nodes')
	plt.plot(small_x, small, 'g^', medium_x, medium, 'bs', big_x, big, 'k>')
	plt.xlim([0,40])
	plt.yscale('log')
	plt.ylabel('Nodes explored')
	plt.xlabel('Number of vertices')
	plt.text(11, 25, 'Small', color='green')
	plt.text(21, 3000, 'Medium', color='blue')
	plt.text(31, 90000, 'Big')
	plt.show()


def plot_total_time(small, medium, big):
	plt.title('Total execution time')
	plt.plot(small_x, small, 'g^', medium_x, medium, 'bs', big_x, big, 'k>')
	plt.xlim([0,40])
	plt.ylabel('Executiont time')
	plt.xlabel('Number of vertices')
	plt.text(11, -0.05, 'Small', color='green')
	plt.text(21, 0.13, 'Medium', color='blue')
	plt.text(31, 1.86, 'Big')
	plt.show()


def plot_node_time(small, medium, big):
	plt.title('Average time per node')
	plt.plot(small_x, small, 'g^', medium_x, medium, 'bs', big_x, big, 'k>')
	plt.xlim([0,40])
	plt.ylabel('Average execution time per node')
	plt.xlabel('Number of vertices')
	plt.text(11, 24000, 'Small', color='green')
	plt.text(21, 30770, 'Medium', color='blue')
	plt.text(31, 26850, 'Big')
	plt.show()
	# Density per node graph
	plt.title('Density per node')
	plt.plot([1, 1, 1, 1, 1], small, 'g^', [2, 2, 2, 2, 2], medium, 'bs', [1.67, 1.67, 1.67, 1.67, 1.67], big, 'k>')
	plt.xlim([0,3])
	plt.ylabel('Average execution time per node')
	plt.xlabel('Number of edges/Number of vertices')
	plt.text(1.05, 24000, 'Small', color='green')
	plt.text(2.1, 30770, 'Medium', color='blue')
	plt.text(1.72, 26850, 'Big')
	plt.show()

def plot_prunes(small_naive, medium_naive, big_naive, small_advanced, medium_advanced, big_advanced):
	plt.title('Näive and advanced prune difference')
	plt.plot(small_x[:3], small_naive, 'r*', medium_x[:3], medium_naive, 'r*', big_x[:3], big_naive, 'r*', markersize=15)
	plt.plot(small_x[:3], small_advanced, 'g*', medium_x[:3], medium_advanced, 'g*', big_x[:3], big_advanced, 'g*', markersize=15)
	plt.xlim([0,40])
	plt.ylabel('Execution time')
	plt.xlabel('Number of vertices')
	plt.show()


if __name__ == '__main__':
	#plot_nodes([35, 47, 17, 42, 35], [2067, 3841, 5493, 2940, 5128], [29215, 324655, 426593, 48081, 240692])
	#plot_total_time([0.001548, 0.001737, 0.003178, 0.001123, 0.002787], [0.374851, 0.013215, 0.152488, 0.025261, 0.136990], 
	#                [1.198830, 0.188396, 3.785129, 2.417790, 0.523870])
	#plot_node_time([23591.93191278172, 24591.408447743364, 25351.978669382563, 23139.398663779273, 23581.00048525997], 
	#               [31428.57902820113, 31015.97177350565, 29781.288398887707, 31143.64149170434, 31655.025670735147], 
	#               [26583.358853071815, 27089.533225405947, 27228.71816523857, 27017.107700707063, 27194.705950019244])
	plot_prunes([0.376184, 0.228277, 0.262616], [162.643532, 181.543734, 190.52352321], [1044.53253, 983.643373, 1203.5436353], 
	            [0.004228, 0.001767, 0.006442], [0.122620, 0.108829, 0.319257], [0.049904, 0.207006, 0.087700])
