% Vertex Coloring
% Álvaro Bermejo
% May 2016

Preface about Python
====================

What is Python
--------------
Python is a strong, dinamically typed interpreted language with an emphasis on 
readibility.

How to get Python
-----------------
Python can be downloaded from [the oficial webpage](https://www.python.org/downloads/), 
alternatively in a Linux system it can be get from the package manager, in 
Debian and derivates via `sudo apt-get install python3`, (Although most of the 
times the language is already installed).

Abstract
========
The vertex coloring problem is a NP-Hard problem with a best known algorithm complexity
of O(2^n) [Björklund, Husfeldt & Koivisto (2009)](http://epubs.siam.org/doi/abs/10.1137/070683933), 
which lends itself nicely to the pruning technique.

Solution Tree
=============

On each level we have to consider the options to use each of the colors we have used 
until that moment, and to use a new one. 

![Problem Tree Structure](img/problem_tree.png)

As we can see because of this the lower we go on the solution tree the wider it gets. 

On each node of the tree we save a dictionary with the nodes we have colored so far as
keys and the colors of each one has the respective values. We also save the number of 
colours we have used until the moment, but that's not necessary, it justs makes some 
cost estimations faster.

Optimistic()
============

Advanced
--------
The advanced estimate is just the number of colors we have created by that 
moment. ∈ O(1) because we save the number of colors on the data structure.

Pessimistic()
=============

Naïve
-----
The naïve pessimistic cost is calculated as the total number of vertices. 
∈ O(1)

Advanced
--------
The pessimistic advanced estimate is given by the number of nodes left plus the 
current number of colors used. ∈ O(n)

Graphs
======

The following graphs follow the same rules, in all the graphics each points 
represents the average of ten executions. This is done because the way we 
represent the graph (with a dictionary) means we can't run the algorithm 
deterministically, the order in which we explore the vertices is different 
each time.

All graphs include three datasets, one small which only has 10 vertices, one 
medium with 20, and a big one with 30 vertices.

![Difference between näive and advanced prune](img/prune_difference.png)

In this graph the red markers represent the executing with the näive techniques 
and the green ones with the advanced. We can see that the difference between 
them is enormous, not only in exxecution time but in memory consumption (Not 
shown on graph)

![Total time](img/total_time.png)

We can see a correlation between problem size (given by number of vertices) and
time of execution, still there were sometimes that a "big" algorithm that was 
faster than the small because the prunning got lucky.

![Explored nodes](img/explored_nodes.png)

It should be noted that the y scale is logarithmic on this graph. The memory 
needed was so big that sometimes it consumed all the RAM (in a computer with
8 Gigabytes).

![Average time per node](img/average_per_node.png)

The units on the time scale are relative to the specific CPU were the benchmarks 
were produced, so they are only useful to compare between executions.

Some key information that is needed to understand this graph is missing, the 
*density* of the graph. The 10 vertices graph has 10 edges, the 20 one has 40 
and the 30 vertices edges has 50, we can infer correlation, in fact we 
can see it on the following graph.

![Density/Time per node](img/density_per_node.png)

Insight
=======

Optimization
------------
On the general prunning techniques scheme some checks like `optimistic(heap[0]) <= best_cost`
are performed with the `<=` operator, however we can convert those checks into 
a `<` if we check if we have found a single solution, if we have we can use the 
`<`, otherwise we stick to `<=` (Although because we check the `<` already we 
can use just a `=`).

Conclusion
----------
Techniques like prunning can make viable solving most instances of a problem 
that is too complex to be solved by regular algorithms, but even with very good 
optimistic and pessimistic cost functions sometimes we can't solve the problem 
on a reasonable time.

In order to make it viable for bigger n something like clique separators and more 
[advanced techniques](http://www.lighterra.com/papers/graphcoloring/), 
which are used in problems such as register allocation.
